package com.ar.flowchallenge.weatherDetail

import com.ar.flowchallenge.model.weatherDetail.ModelAtCity
import com.ar.flowchallenge.model.weatherDetail.ModelWeatherDetailResponse
import com.ar.flowchallenge.network.weatherDetail.WeatherDetailAdapter
import com.ar.flowchallenge.network.weatherDetail.WeatherDetailListener
import com.ar.flowchallenge.ui.weatherDetail.WeatherDetailPresenter
import com.ar.flowchallenge.ui.weatherDetail.WeatherDetailView
import com.nhaarman.mockitokotlin2.*
import org.junit.Before
import org.junit.Test

class WeatherDetailFragmentPresenterTest {

    private lateinit var view: WeatherDetailView
    private lateinit var presenter: WeatherDetailPresenter

    @Before
    fun createInstances() {
        view = mock()
        presenter = WeatherDetailPresenter(view)
    }

    @Test
    fun `given service error when service is call then hide recycler view data`() {
        // GIVEN
        val adapter: WeatherDetailAdapter = mock {
            on { weatherDetailService(any(), any()) } doAnswer {
                it.getArgument<WeatherDetailListener>(1).onErrorResponse()
                mock()
            }
        }
        presenter.adapter = adapter

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showServiceError()
    }

    @Test
    fun `given empty data error when service is call then hide recycler view data`() {
        // GIVEN
        val adapter: WeatherDetailAdapter = mock {
            on { weatherDetailService(any(), any()) } doAnswer {
                it.getArgument<WeatherDetailListener>(1).onEmptyResponse()
                mock()
            }
        }
        presenter.adapter = adapter

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showEmptyDataError()
    }

    @Test
    fun `given success response when service is call then show recycler view data`() {
        // GIVEN
        val city = ModelAtCity(ID, NAME, COUNTRY)
        val data = ModelWeatherDetailResponse(CODE, mutableListOf(), city)
        val adapter: WeatherDetailAdapter = mock {
            on { weatherDetailService(any(), any()) } doAnswer {
                it.getArgument<WeatherDetailListener>(1).onSuccessResponse(data)
                mock()
            }
        }
        presenter.adapter = adapter

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showData(data.details)
    }

    companion object {
        private const val CODE = 10
        private const val ID = 100
        private const val NAME = "name"
        private const val COUNTRY = "country"
    }
}