package com.ar.flowchallenge.currentWeather

import com.ar.flowchallenge.model.currentWeather.ModelAtMain
import com.ar.flowchallenge.model.currentWeather.ModelAtWeather
import com.ar.flowchallenge.model.currentWeather.ModelCurrentWeather
import com.ar.flowchallenge.model.currentWeather.ModelCurrentWeatherResponse
import com.ar.flowchallenge.network.currentWeather.CurrentWeatherAdapter
import com.ar.flowchallenge.network.currentWeather.CurrentWeatherListener
import com.ar.flowchallenge.ui.currentWeather.CurrentWeatherPresenter
import com.ar.flowchallenge.ui.currentWeather.CurrentWeatherView
import com.nhaarman.mockitokotlin2.*
import org.junit.Test

class CurrentWeatherFragmentPresenterTest {

    private lateinit var view: CurrentWeatherView
    private lateinit var presenter: CurrentWeatherPresenter

    @Test
    fun `given empty data arguments when presenter init then show error screen`() {
        // GIVEN
        val data = ModelCurrentWeather("", "", "", mutableListOf())
        view = mock() {
            on { getArgumentData() } doReturn data
        }
        presenter = CurrentWeatherPresenter(view)

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showServiceError()
    }

    @Test
    fun `given success adapter response when presenter init then show weather data`() {
        // GIVEN
        val weather = mutableListOf<ModelAtWeather>().apply {
            add(ModelAtWeather(ID, TEXT, TEXT))
        }
        val main = ModelAtMain(TEMP, TEMP, TEMP, TEMP, TEMP, ID)
        val response = ModelCurrentWeatherResponse(weather, main, ID, TEXT)
        val data = ModelCurrentWeather(COUNTRY, PROVINCE, CAPITAL, mutableListOf())
        view = mock() {
            on { getArgumentData() } doReturn data
        }
        val adapter: CurrentWeatherAdapter = mock() {
            on { currentWeatherService(any(), any(), any()) } doAnswer {
                it.getArgument<CurrentWeatherListener>(2).onSuccessResponse(response)
                mock()
            }
        }
        presenter = CurrentWeatherPresenter(view).apply {
            this.adapter = adapter
        }

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showWeatherData(any())
    }

    companion object {
        private const val COUNTRY = "Argentina"
        private const val PROVINCE = "Buenos Aires"
        private const val CAPITAL = "La Plata"
        private const val ID = 10
        private const val TEXT = "rain"
        private const val TEMP = 10.0
    }
}