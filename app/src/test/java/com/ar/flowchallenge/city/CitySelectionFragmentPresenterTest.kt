package com.ar.flowchallenge.city

import com.ar.flowchallenge.model.city.ModelCity
import com.ar.flowchallenge.model.city.ModelJsonData
import com.ar.flowchallenge.model.city.ModelProvince
import com.ar.flowchallenge.ui.city.CitySelectionPresenter
import com.ar.flowchallenge.ui.city.CitySelectionView
import com.ar.flowchallenge.utils.jsonUtils.JsonFromAssetsListener
import com.ar.flowchallenge.utils.jsonUtils.JsonFromAssetsParser
import com.nhaarman.mockitokotlin2.*
import org.junit.Test

class CitySelectionFragmentPresenterTest {

    private lateinit var view: CitySelectionView
    private lateinit var presenter: CitySelectionPresenter

    @Test
    fun `given exception json parse value when presenter init then json error view is showed`() {
        // GIVE
        view = mock() {
            on { getCitySelectionContext() } doReturn mock()
        }
        val jsonFromAssetsParser: JsonFromAssetsParser = mock() {
            on { getCitiesFromAssets(any(), any()) } doAnswer {
                it.getArgument<JsonFromAssetsListener>(1).onErrorParse()
                mock()
            }
        }
        presenter = CitySelectionPresenter(view).apply {
            jsonAdapter = jsonFromAssetsParser
        }

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showJsonParseError()
    }

    @Test
    fun `given success json parse value when presenter init then spinner data is showed`() {
        // GIVE
        val cities = mutableListOf<ModelCity>().apply {
            add(ModelCity(CITY, false))
        }
        val provinces = mutableListOf<ModelProvince>().apply {
            add(ModelProvince(PROVINCE, CAPITAL, cities))
        }
        val data = ModelJsonData(COUNTRY, provinces)
        val spinnerData = mutableListOf<String>().apply {
            add(DEFAULT_VALUE)
            data.data.forEach { item ->
                add(item.province)
            }
        }
        view = mock() {
            on { getCitySelectionContext() } doReturn mock()
            on { getDefaultValue() } doReturn DEFAULT_VALUE
        }
        val jsonFromAssetsParser: JsonFromAssetsParser = mock() {
            on { getCitiesFromAssets(any(), any()) } doAnswer {
                it.getArgument<JsonFromAssetsListener>(1).onSuccessParser(data)
                mock()
            }
        }
        presenter = CitySelectionPresenter(view).apply {
            jsonAdapter = jsonFromAssetsParser
        }

        // WHEN
        presenter.onAttachView()

        // THEN
        verify(view, times(1)).showSpinnerData(spinnerData)
    }

    @Test
    fun `given default value when spinner item is selected then hide city and button views`() {
        // GIVEN
        val provinces = mutableListOf<String>().apply {
            add(DEFAULT_VALUE)
        }
        view = mock() {
            on { getDefaultValue() } doReturn DEFAULT_VALUE
        }
        presenter = CitySelectionPresenter(view)

        // WHEN
        presenter.onSpinnerItemSelected(provinces, 0)

        // THEN
        verify(view, times(1)).toggleCitiesData(false)
        verify(view, times(1)).toggleConfirmButtonStatus(false)
    }

    @Test
    fun `given accepted value when spinner item is selected then show city and button views`() {
        // GIVE
        val cities = mutableListOf<ModelCity>().apply {
            add(ModelCity(CITY, false))
        }
        val provinces = mutableListOf<ModelProvince>().apply {
            add(ModelProvince(PROVINCE, CAPITAL, cities))
        }
        val data = ModelJsonData(COUNTRY, provinces)
        val spinnerData = mutableListOf<String>().apply {
            add(DEFAULT_VALUE)
            data.data.forEach { item ->
                add(item.province)
            }
        }
        view = mock() {
            on { getCitySelectionContext() } doReturn mock()
            on { getDefaultValue() } doReturn DEFAULT_VALUE
        }
        val jsonFromAssetsParser: JsonFromAssetsParser = mock() {
            on { getCitiesFromAssets(any(), any()) } doAnswer {
                it.getArgument<JsonFromAssetsListener>(1).onSuccessParser(data)
                mock()
            }
        }
        presenter = CitySelectionPresenter(view).apply {
            jsonAdapter = jsonFromAssetsParser
            onAttachView()
        }

        // WHEN
        presenter.onSpinnerItemSelected(spinnerData, 1)

        // THEN
        verify(view, times(1)).toggleCitiesData(true)
        verify(view, times(1)).toggleConfirmButtonStatus(true)
    }

    companion object {
        private const val COUNTRY = "Argentina"
        private const val PROVINCE = "Buenos Aires"
        private const val CAPITAL = "La Plata"
        private const val CITY = "San Martin"
        private const val DEFAULT_VALUE = "-None-"
    }
}