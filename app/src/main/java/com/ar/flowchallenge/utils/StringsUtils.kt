package com.ar.flowchallenge.utils

fun Double.round(): String = String.format("%.2f", this)

fun Int.round(): String = this.toString()