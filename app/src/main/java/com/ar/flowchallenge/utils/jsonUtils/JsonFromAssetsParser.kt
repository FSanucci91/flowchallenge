package com.ar.flowchallenge.utils.jsonUtils

import android.content.Context
import com.ar.flowchallenge.model.city.ModelJsonData
import com.google.gson.GsonBuilder
import java.lang.Exception

class JsonFromAssetsParser {

    fun getCitiesFromAssets(context: Context, listener: JsonFromAssetsListener) {
        try {
            val inputStream = context.assets.open(JSON_FILE)
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.run {
                read(buffer)
                close()
            }

            val json = String(buffer)
            val gson = GsonBuilder().create()
            val dataParsed = gson.fromJson(json, ModelJsonData::class.java)
            listener.onSuccessParser(dataParsed)
        } catch (ex: Exception) {
            ex.printStackTrace()
            listener.onErrorParse()
        }
    }

    companion object {
        private const val JSON_FILE = "cities.json"
    }
}