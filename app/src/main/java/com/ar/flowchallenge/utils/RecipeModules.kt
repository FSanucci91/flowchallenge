package com.ar.flowchallenge.utils

import com.ar.flowchallenge.utils.koin.ToastFactory
import com.ar.flowchallenge.utils.koin.koinModules
import org.koin.core.KoinApplication
import org.koin.core.module.Module
import org.koin.dsl.module

object RecipeModules {

    private val appModule: Module = module {
        single { ToastFactory(get()) }
    }

    fun KoinApplication.initializeModules() = modules(
        listOf(
            appModule,
            koinModules
        )
    )
}