package com.ar.flowchallenge.utils.koin

import com.ar.flowchallenge.ui.city.CitySelectionFragment
import com.ar.flowchallenge.ui.city.CitySelectionPresenter
import com.ar.flowchallenge.ui.city.CitySelectionView
import com.ar.flowchallenge.ui.currentWeather.CurrentWeatherFragment
import com.ar.flowchallenge.ui.currentWeather.CurrentWeatherPresenter
import com.ar.flowchallenge.ui.currentWeather.CurrentWeatherView
import com.ar.flowchallenge.ui.weatherDetail.WeatherDetailFragment
import com.ar.flowchallenge.ui.weatherDetail.WeatherDetailPresenter
import com.ar.flowchallenge.ui.weatherDetail.WeatherDetailView
import org.koin.core.qualifier.named
import org.koin.dsl.module

val koinModules = module {

    // 1. City Selection View
    factory { CitySelectionFragment() }
    scope(named<CitySelectionFragment>()) {
        scoped { (view: CitySelectionView) -> CitySelectionPresenter(view) }
    }

    // 2. Current Weather View
    factory { CurrentWeatherFragment() }
    scope(named<CurrentWeatherFragment>()) {
        scoped { (view: CurrentWeatherView) -> CurrentWeatherPresenter(view) }
    }

    // 3. Weather Detail View
    factory { WeatherDetailFragment() }
    scope(named<WeatherDetailFragment>()) {
        scoped { (view: WeatherDetailView) -> WeatherDetailPresenter(view) }
    }
}