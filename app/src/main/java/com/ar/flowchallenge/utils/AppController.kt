package com.ar.flowchallenge.utils

import com.ar.flowchallenge.core.BaseApplication
import com.ar.flowchallenge.utils.RecipeModules.initializeModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class AppController : BaseApplication() {

    override fun onInit() {

        startKoin {
            androidContext(this@AppController)
            initializeModules()
        }
    }
}