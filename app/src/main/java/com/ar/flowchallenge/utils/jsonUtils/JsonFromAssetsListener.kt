package com.ar.flowchallenge.utils.jsonUtils

import com.ar.flowchallenge.model.city.ModelJsonData

interface JsonFromAssetsListener {
    fun onSuccessParser(dataParsed: ModelJsonData)

    fun onErrorParse()
}