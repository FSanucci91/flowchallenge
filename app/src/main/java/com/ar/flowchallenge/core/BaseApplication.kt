package com.ar.flowchallenge.core

import android.app.Application

abstract class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        onInit()
    }

    abstract fun onInit()
}