package com.ar.flowchallenge.core

interface BasePresenter<T> {
    val view: T

    fun onAttachView()
}
