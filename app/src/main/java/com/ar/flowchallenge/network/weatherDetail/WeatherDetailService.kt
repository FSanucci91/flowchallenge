package com.ar.flowchallenge.network.weatherDetail

import com.ar.flowchallenge.model.weatherDetail.ModelWeatherDetailResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherDetailService {

    @GET("forecast")
    fun getForecastWeather(@Query(ID) cityId: Int, @Query(KEY) appId: String): Call<ModelWeatherDetailResponse>

    companion object {
        private const val ID = "id"
        private const val KEY = "appid"
    }
}