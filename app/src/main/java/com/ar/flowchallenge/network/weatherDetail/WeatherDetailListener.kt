package com.ar.flowchallenge.network.weatherDetail

import com.ar.flowchallenge.model.weatherDetail.ModelWeatherDetailResponse

interface WeatherDetailListener {
    fun onSuccessResponse(data: ModelWeatherDetailResponse)

    fun onEmptyResponse()

    fun onErrorResponse()
}