package com.ar.flowchallenge.network.currentWeather

import com.ar.flowchallenge.model.currentWeather.ModelCurrentWeatherResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrentWeatherService {

    @GET("weather")
    fun getCurrentWeather(@Query(CITY) city: String, @Query(KEY) id: String): Call<ModelCurrentWeatherResponse>

    companion object {
        private const val CITY = "q"
        private const val KEY = "appid"
    }
}