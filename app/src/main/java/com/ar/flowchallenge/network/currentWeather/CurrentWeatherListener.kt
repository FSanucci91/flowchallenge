package com.ar.flowchallenge.network.currentWeather

import com.ar.flowchallenge.model.currentWeather.ModelCurrentWeatherResponse

interface CurrentWeatherListener {
    fun onSuccessResponse(data: ModelCurrentWeatherResponse)

    fun onEmptyResponse()

    fun onErrorResponse()
}