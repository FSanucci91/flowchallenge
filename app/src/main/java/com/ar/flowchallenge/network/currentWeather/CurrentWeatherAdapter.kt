package com.ar.flowchallenge.network.currentWeather

import com.ar.flowchallenge.model.currentWeather.ModelCurrentWeatherResponse
import com.ar.flowchallenge.network.core.RetrofitService
import com.ar.flowchallenge.utils.Configuration
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CurrentWeatherAdapter() {

    fun currentWeatherService(
        country: String,
        city: String,
        listener: CurrentWeatherListener
    ) {
        val cityQuery = "$city,$country"
        RetrofitService()
            .getInstance()
            .create(CurrentWeatherService::class.java)
            .getCurrentWeather(cityQuery, Configuration.KEY)
            .enqueue(object : Callback<ModelCurrentWeatherResponse> {
                override fun onResponse(
                    call: Call<ModelCurrentWeatherResponse>,
                    response: Response<ModelCurrentWeatherResponse>
                ) {
                    response.body()?.let {
                        with(it.main) {
                            temperature -= KELVIN
                            thermalSensation -= KELVIN
                            tempMin -= KELVIN
                            tempMax -= KELVIN
                        }
                        listener.onSuccessResponse(it)
                    } ?: run {
                        listener.onEmptyResponse()
                    }
                }

                override fun onFailure(call: Call<ModelCurrentWeatherResponse>, t: Throwable) {
                    listener.onErrorResponse()
                }
            })
    }

    companion object {
        private const val KELVIN = 273.0
    }
}