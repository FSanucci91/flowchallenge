package com.ar.flowchallenge.network.weatherDetail

import com.ar.flowchallenge.model.weatherDetail.ModelWeatherDetailResponse
import com.ar.flowchallenge.network.core.RetrofitService
import com.ar.flowchallenge.utils.Configuration
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WeatherDetailAdapter {

    fun weatherDetailService(
        cityId: Int,
        listener: WeatherDetailListener
    ) {
        RetrofitService()
            .getInstance()
            .create(WeatherDetailService::class.java)
            .getForecastWeather(cityId, Configuration.KEY)
            .enqueue(object : Callback<ModelWeatherDetailResponse> {
                override fun onResponse(
                    call: Call<ModelWeatherDetailResponse>,
                    response: Response<ModelWeatherDetailResponse>
                ) {
                    response.body()?.let {
                        listener.onSuccessResponse(it)
                    } ?: run {
                        listener.onEmptyResponse()
                    }
                }

                override fun onFailure(call: Call<ModelWeatherDetailResponse>, t: Throwable) {
                    listener.onErrorResponse()
                }
            })
    }
}