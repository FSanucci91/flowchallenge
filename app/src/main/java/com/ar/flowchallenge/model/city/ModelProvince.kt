package com.ar.flowchallenge.model.city

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ModelProvince(
    @SerializedName("province")
    val province: String,
    @SerializedName("capital")
    val capital: String,
    @SerializedName("cities")
    val cities: List<ModelCity>
) : Serializable