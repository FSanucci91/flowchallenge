package com.ar.flowchallenge.model.weatherDetail

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ModelAtCity(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("country")
    val country: String
) : Serializable