package com.ar.flowchallenge.model.city

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ModelCity(
    @SerializedName("city")
    val city: String,
    var selected: Boolean = false
) : Serializable