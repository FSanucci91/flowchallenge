package com.ar.flowchallenge.model.currentWeather

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ModelAtMain(
    @SerializedName("temp")
    var temperature: Double,
    @SerializedName("feels_like")
    var thermalSensation: Double,
    @SerializedName("temp_min")
    var tempMin: Double,
    @SerializedName("temp_max")
    var tempMax: Double,
    @SerializedName("pressure")
    val pressure: Double,
    @SerializedName("humidity")
    val humidity: Int
) : Serializable