package com.ar.flowchallenge.model.weatherDetail

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ModelAtWeather(
    @SerializedName("id")
    val id: Int,
    @SerializedName("main")
    val main: String,
    @SerializedName("description")
    val description: String
) : Serializable