package com.ar.flowchallenge.model.weatherDetail

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ModelAtMain(
    @SerializedName("temp")
    val temperature: Double,
    @SerializedName("temp_min")
    val tempMin: Double,
    @SerializedName("temp_max")
    val tempMax: Double,
    @SerializedName("feels_like")
    val feelsLike: Double,
    @SerializedName("pressure")
    val pressure: Double,
    @SerializedName("humidity")
    val humidity: Int
) : Serializable