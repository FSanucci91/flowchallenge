package com.ar.flowchallenge.model.currentWeather

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ModelCurrentWeatherResponse(
    @SerializedName("weather")
    val weather: List<ModelAtWeather>,
    @SerializedName("main")
    val main: ModelAtMain,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String
) : Serializable