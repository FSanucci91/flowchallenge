package com.ar.flowchallenge.model.currentWeather

import com.ar.flowchallenge.model.city.ModelCity
import java.io.Serializable

data class ModelCurrentWeather(
    val country: String,
    val province: String,
    val capital: String,
    val cities: List<ModelCity>
) : Serializable