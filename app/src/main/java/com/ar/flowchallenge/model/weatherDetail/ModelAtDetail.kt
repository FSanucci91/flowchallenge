package com.ar.flowchallenge.model.weatherDetail

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ModelAtDetail(
    @SerializedName("main")
    val main: ModelAtMain,
    @SerializedName("weather")
    val weather: List<ModelAtWeather>,
    @SerializedName("dt_txt")
    val time: String
) : Serializable



