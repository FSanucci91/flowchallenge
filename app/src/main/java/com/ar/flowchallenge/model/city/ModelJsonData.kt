package com.ar.flowchallenge.model.city

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ModelJsonData(
    @SerializedName("country")
    val country: String,
    @SerializedName("data")
    val data: List<ModelProvince>
) : Serializable