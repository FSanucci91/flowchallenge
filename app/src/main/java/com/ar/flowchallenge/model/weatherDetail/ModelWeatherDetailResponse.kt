package com.ar.flowchallenge.model.weatherDetail

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ModelWeatherDetailResponse(
    @SerializedName("cod")
    val code: Int,
    @SerializedName("list")
    val details: List<ModelAtDetail>,
    @SerializedName("city")
    val city: ModelAtCity
) : Serializable