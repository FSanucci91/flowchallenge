package com.ar.flowchallenge.ui.currentWeather

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.ar.flowchallenge.R
import com.ar.flowchallenge.core.BaseFragment
import com.ar.flowchallenge.model.city.ModelCity
import com.ar.flowchallenge.model.currentWeather.ModelCurrentWeather
import com.ar.flowchallenge.model.currentWeather.ModelCurrentWeatherResponse
import com.ar.flowchallenge.ui.city.CitySelectionActivity
import com.ar.flowchallenge.ui.weatherDetail.WeatherDetailActivity
import kotlinx.android.synthetic.main.fragment_current_weather.*
import kotlinx.android.synthetic.main.layout_error.*
import org.koin.android.scope.currentScope
import org.koin.core.parameter.parametersOf

class CurrentWeatherFragment : BaseFragment<CurrentWeatherPresenter>(), CurrentWeatherView {

    override val presenter: CurrentWeatherPresenter by currentScope.inject { parametersOf(this) }

    override val layout: Int = R.layout.fragment_current_weather

    private lateinit var modelCurrentWeather: ModelCurrentWeather
    private lateinit var listAdapter: CurrentWeatherListAdapter

    override fun init() {
        with(arguments!!) {
            modelCurrentWeather =
                ModelCurrentWeather(
                    getString(CurrentWeatherActivity.COUNTRY)!!,
                    getString(CurrentWeatherActivity.PROVINCE)!!,
                    getString(CurrentWeatherActivity.CAPITAL)!!,
                    if (containsKey(CurrentWeatherActivity.CITIES)) getSerializable(
                        CurrentWeatherActivity.CITIES
                    )!! as List<ModelCity>
                    else listOf()
                )
        }

        listAdapter =
            CurrentWeatherListAdapter(mutableListOf(), object : CurrentWeatherListListener {
                override fun onItemClick(position: Int) {
                    presenter.onCardItemButtonClicked(position)
                }
            })

        vRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
        }
    }

    override fun setListeners() {
        vErrorButton.setOnClickListener {
            CitySelectionActivity.start(requireContext())
            requireActivity().finish()
        }
    }

    override fun getArgumentData(): ModelCurrentWeather = modelCurrentWeather

    override fun showWeatherData(data: List<ModelCurrentWeatherResponse>) {
        listAdapter.updateCardIssuers(data)
        vRecyclerView.visibility = View.VISIBLE
    }

    override fun toggleProgressBar(active: Boolean) {
        vProgressBar.visibility = if (active) View.VISIBLE else View.GONE
    }

    override fun showServicePartialError() {
        toastFactory.show(R.string.current_weather_service_partial_error)
    }

    override fun showServiceError() {
        vErrorContainer.visibility = View.VISIBLE
        vSubtitleError.text = getString(R.string.current_weather_service_error)
    }

    override fun goToWeatherDetail(cityId: Int) {
        WeatherDetailActivity.start(requireContext(), cityId)
    }

    companion object {
        fun newInstance(context: Context, arguments: Bundle?) =
            instantiate(
                context,
                CurrentWeatherFragment::class.java.name,
                arguments
            ) as CurrentWeatherFragment
    }
}