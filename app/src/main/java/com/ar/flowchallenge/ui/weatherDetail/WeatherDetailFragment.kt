package com.ar.flowchallenge.ui.weatherDetail

import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.ar.flowchallenge.R
import com.ar.flowchallenge.core.BaseFragment
import com.ar.flowchallenge.model.weatherDetail.ModelAtDetail
import kotlinx.android.synthetic.main.fragment_weather_detail.*
import kotlinx.android.synthetic.main.layout_error.*
import org.koin.android.scope.currentScope
import org.koin.core.parameter.parametersOf

class WeatherDetailFragment : BaseFragment<WeatherDetailPresenter>(), WeatherDetailView {

    override val presenter: WeatherDetailPresenter by currentScope.inject { parametersOf(this) }

    override val layout: Int = R.layout.fragment_weather_detail

    private var cityId: Int = 0
    private lateinit var listAdapter: WeatherDetailListAdapter

    override fun init() {
        cityId = arguments!!.getInt(WeatherDetailActivity.CITY_ID)
        setRecyclerViewData()
    }

    private fun setRecyclerViewData() {
        listAdapter = WeatherDetailListAdapter(mutableListOf())

        vRecyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = listAdapter
        }

        LinearSnapHelper().apply { attachToRecyclerView(vRecyclerView) }

        val cardMargin: Int = resources.getDimension(R.dimen.space_min).toInt()
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)

        vRecyclerView.addItemDecoration(ItemDecoration(cardMargin, displayMetrics.widthPixels))
    }

    override fun setListeners() {
        vConfirmBtn.setOnClickListener {
            requireActivity().finish()
        }

        vErrorButton.setOnClickListener {
            requireActivity().finish()
        }
    }

    override fun toggleProgressBar(active: Boolean) {
        vProgressBar.visibility = if (active) View.VISIBLE else View.GONE
    }

    override fun getCityId(): Int = cityId

    override fun showEmptyDataError() {
        showError(getString(R.string.weather_detail_empty_data_error))
    }

    override fun showServiceError() {
        showError(getString(R.string.weather_detail_service_error))
    }

    private fun showError(message: String) {
        vErrorContainer.visibility = View.VISIBLE
        vConfirmBtn.visibility = View.GONE
        vRecyclerView.visibility = View.GONE
        vSubtitleError.text = message
    }

    override fun showData(data: List<ModelAtDetail>) {
        listAdapter.updateInstallments(data)
        vRecyclerView.visibility = View.VISIBLE
    }

    companion object {
        fun newInstance(context: Context, arguments: Bundle?) =
            instantiate(
                context,
                WeatherDetailFragment::class.java.name,
                arguments
            ) as WeatherDetailFragment
    }
}