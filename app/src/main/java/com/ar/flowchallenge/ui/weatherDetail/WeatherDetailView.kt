package com.ar.flowchallenge.ui.weatherDetail

import com.ar.flowchallenge.model.weatherDetail.ModelAtDetail

interface WeatherDetailView {
    fun toggleProgressBar(active: Boolean)

    fun getCityId(): Int

    fun showEmptyDataError()

    fun showServiceError()

    fun showData(data: List<ModelAtDetail>)
}