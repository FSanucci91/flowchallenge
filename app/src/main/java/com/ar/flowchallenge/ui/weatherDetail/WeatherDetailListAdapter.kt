package com.ar.flowchallenge.ui.weatherDetail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ar.flowchallenge.R
import com.ar.flowchallenge.model.weatherDetail.ModelAtDetail
import com.ar.flowchallenge.utils.round
import kotlinx.android.synthetic.main.item_weather_detail.view.*

class WeatherDetailListAdapter(
    var forecastWeather: MutableList<ModelAtDetail>
) : RecyclerView.Adapter<WeatherDetailListAdapter.WeatherDetailViewHolder>() {

    fun updateInstallments(newForecastWeather: List<ModelAtDetail>) {
        forecastWeather.run {
            clear()
            addAll(newForecastWeather)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        p1: Int
    ) = WeatherDetailViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_weather_detail,
            parent,
            false
        )
    )

    override fun getItemCount(): Int = forecastWeather.size

    override fun onBindViewHolder(holder: WeatherDetailViewHolder, position: Int) {
        holder.bind(forecastWeather[position])
    }

    class WeatherDetailViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(weather: ModelAtDetail) {
            view.run {
                vDate.text = weather.time
                val temperature = weather.main.temperature - KELVIN
                vTemperatureText.text = context.getString(
                    R.string.weather_temperature,
                    temperature.round()
                )
                val temperatureLow = weather.main.tempMin - KELVIN
                vTemperatureLowText.text = context.getString(
                    R.string.weather_low_temperature,
                    temperatureLow.round()
                )
                val temperatureHigh = weather.main.tempMax - KELVIN
                vTemperatureHighText.text = context.getString(
                    R.string.weather_high_temperature,
                    temperatureHigh.round()
                )
                vPressureText.text = context.getString(
                    R.string.weather_pressure,
                    weather.main.pressure.round()
                )
                vHumidityText.text = context.getString(
                    R.string.weather_humidity,
                    weather.main.humidity.round()
                )
                val feelsLike = weather.main.feelsLike - KELVIN
                vFeelsLikeText.text = context.getString(
                    R.string.weather_feels_like_value,
                    feelsLike.round()
                )
                if (weather.weather.isNotEmpty()) {
                    vWeatherMain.text = weather.weather[0].main
                    vWeatherDescription.text = weather.weather[0].description
                } else {
                    vWeatherTitle.visibility = View.GONE
                    vWeatherMain.visibility = View.GONE
                    vWeatherDescription.visibility = View.GONE
                }
            }
        }
    }

    companion object {
        private const val KELVIN = 273.0
    }
}