package com.ar.flowchallenge.ui.city

import android.content.Context
import com.ar.flowchallenge.model.city.ModelCity

interface CitySelectionView {
    fun getDefaultValue(): String

    fun getCitySelectionContext(): Context

    fun toggleProgressBar(status: Boolean)

    fun toggleDataProgressBar(status: Boolean)

    fun showSpinnerData(data: List<String>)

    fun showJsonParseError()

    fun toggleConfirmButtonStatus(active: Boolean)

    fun toggleCitiesData(active: Boolean)

    fun showCitiesData(cities: List<ModelCity>)

    fun showMaxLimitReachedError()

    fun goToCurrentWeatherActivity(
        country: String,
        province: String,
        capital: String,
        itemsSelected: List<ModelCity>
    )
}