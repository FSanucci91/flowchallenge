package com.ar.flowchallenge.ui.city

interface CityListListener {
    fun onItemClick(position: Int, active: Boolean)
}