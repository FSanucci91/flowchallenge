package com.ar.flowchallenge.ui.currentWeather

import com.ar.flowchallenge.model.currentWeather.ModelCurrentWeather
import com.ar.flowchallenge.model.currentWeather.ModelCurrentWeatherResponse

interface CurrentWeatherView {
    fun getArgumentData(): ModelCurrentWeather

    fun showWeatherData(data: List<ModelCurrentWeatherResponse>)

    fun toggleProgressBar(active: Boolean)

    fun showServicePartialError()

    fun showServiceError()

    fun goToWeatherDetail(cityId: Int)
}