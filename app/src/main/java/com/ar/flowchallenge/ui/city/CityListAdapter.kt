package com.ar.flowchallenge.ui.city

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ar.flowchallenge.R
import com.ar.flowchallenge.model.city.ModelCity
import kotlinx.android.synthetic.main.item_city.view.*

class CityListAdapter(
    var cities: MutableList<ModelCity>,
    private val listener: CityListListener
) : RecyclerView.Adapter<CityListAdapter.CityViewHolder>() {

    fun updateCardIssuers(newCities: List<ModelCity>) {
        cities.run {
            clear()
            addAll(newCities)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        p1: Int
    ) = CityViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_city,
            parent,
            false
        )
    )

    override fun getItemCount(): Int = cities.size

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        holder.bind(cities[position], position, listener)
    }

    class CityViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(city: ModelCity, position: Int, listener: CityListListener) {
            view.run {
                vCardName.text = city.city
                vItemSelected.run {
                    setOnClickListener {
                        listener.onItemClick(position, vItemSelected.isChecked)
                    }
                    isChecked = city.selected
                }
            }
        }
    }
}