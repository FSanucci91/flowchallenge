package com.ar.flowchallenge.ui.weatherDetail

import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ItemDecoration(private val cardMargin: Int, private val widthPixels: Int) :
    RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)

        val offset = (((widthPixels / 2f) - (view.layoutParams.width / 2f)).toInt())

        val position = parent.getChildAdapterPosition(view)
        if (position == 0) {

            //FirstElement
            (view.layoutParams as ViewGroup.MarginLayoutParams).leftMargin = 0
            outRect.left = offset

            if (state.itemCount >= 2) {
                outRect.right = (cardMargin / 2)
            }
        } else if (position == (state.itemCount - 1)) {

            //LastElement
            (view.layoutParams as ViewGroup.MarginLayoutParams).rightMargin = 0
            outRect.right = offset

            if (state.itemCount >= 2) {
                outRect.left = (cardMargin / 2)
            }
        } else {
            outRect.left = (cardMargin / 2)
            outRect.right = (cardMargin / 2)
        }
    }
}