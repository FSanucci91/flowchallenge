package com.ar.flowchallenge.ui.weatherDetail

import com.ar.flowchallenge.core.BasePresenter
import com.ar.flowchallenge.model.weatherDetail.ModelWeatherDetailResponse
import com.ar.flowchallenge.network.weatherDetail.WeatherDetailAdapter
import com.ar.flowchallenge.network.weatherDetail.WeatherDetailListener

class WeatherDetailPresenter(
    override val view: WeatherDetailView
) : BasePresenter<WeatherDetailView> {

    var adapter = WeatherDetailAdapter()
    private lateinit var forecastWeather: ModelWeatherDetailResponse

    override fun onAttachView() {
        view.toggleProgressBar(true)

        adapter.weatherDetailService(view.getCityId(), object : WeatherDetailListener {
            override fun onSuccessResponse(data: ModelWeatherDetailResponse) {
                forecastWeather = data
                view.run {
                    showData(data.details)
                    toggleProgressBar(false)
                }
            }

            override fun onEmptyResponse() {
                view.run {
                    showEmptyDataError()
                    toggleProgressBar(false)
                }
            }

            override fun onErrorResponse() {
                view.run {
                    showServiceError()
                    toggleProgressBar(false)
                }
            }
        })
    }
}