package com.ar.flowchallenge.ui.currentWeather

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ar.flowchallenge.R
import com.ar.flowchallenge.model.currentWeather.ModelCurrentWeatherResponse
import com.ar.flowchallenge.utils.round
import kotlinx.android.synthetic.main.item_current_weather.view.*

class CurrentWeatherListAdapter(
    var cities: MutableList<ModelCurrentWeatherResponse>,
    private val listener: CurrentWeatherListListener
) : RecyclerView.Adapter<CurrentWeatherListAdapter.CurrentWeatherViewHolder>() {

    fun updateCardIssuers(newCities: List<ModelCurrentWeatherResponse>) {
        cities.run {
            clear()
            addAll(newCities)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        p1: Int
    ) = CurrentWeatherViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_current_weather,
            parent,
            false
        )
    )

    override fun getItemCount(): Int = cities.size

    override fun onBindViewHolder(holder: CurrentWeatherViewHolder, position: Int) {
        holder.bind(cities[position], position, listener)
    }

    class CurrentWeatherViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(
            city: ModelCurrentWeatherResponse,
            position: Int,
            listener: CurrentWeatherListListener
        ) {
            view.run {
                vDate.text = city.name
                vTemperatureText.text = context.getString(
                    R.string.weather_temperature,
                    city.main.temperature.round()
                )
                vTemperatureLowText.text = context.getString(
                    R.string.weather_low_temperature,
                    city.main.tempMin.round()
                )
                vTemperatureHighText.text = context.getString(
                    R.string.weather_high_temperature,
                    city.main.tempMax.round()
                )
                vPressureText.text = context.getString(
                    R.string.weather_pressure,
                    city.main.pressure.round()
                )
                vHumidityText.text = context.getString(
                    R.string.weather_humidity,
                    city.main.pressure.round()
                )
                vCityDetailButton.setOnClickListener {
                    listener.onItemClick(position)
                }
            }
        }
    }
}