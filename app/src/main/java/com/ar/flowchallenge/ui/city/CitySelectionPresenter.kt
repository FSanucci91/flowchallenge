package com.ar.flowchallenge.ui.city

import com.ar.flowchallenge.core.BasePresenter
import com.ar.flowchallenge.model.city.ModelCity
import com.ar.flowchallenge.model.city.ModelJsonData
import com.ar.flowchallenge.model.city.ModelProvince
import com.ar.flowchallenge.utils.jsonUtils.JsonFromAssetsListener
import com.ar.flowchallenge.utils.jsonUtils.JsonFromAssetsParser

class CitySelectionPresenter(
    override val view: CitySelectionView
) : BasePresenter<CitySelectionView> {

    private lateinit var cities: ModelJsonData
    private var itemSpinnerSelected: ModelProvince? = null
    private var itemsSelectedCount: Int = 0
    private val itemsSelected = mutableListOf<ModelCity>()
    var jsonAdapter = JsonFromAssetsParser()

    override fun onAttachView() {
        view.toggleProgressBar(true)

        jsonAdapter.getCitiesFromAssets(
            view.getCitySelectionContext(),
            object : JsonFromAssetsListener {
                override fun onSuccessParser(dataParsed: ModelJsonData) {
                    cities = dataParsed

                    val provinces = mutableListOf<String>().apply {
                        add(view.getDefaultValue())
                    }
                    cities.data.forEach {
                        provinces.add(it.province)
                    }

                    view.run {
                        showSpinnerData(provinces)
                        toggleProgressBar(false)
                    }
                }

                override fun onErrorParse() {
                    view.run {
                        showJsonParseError()
                        toggleProgressBar(false)
                    }
                }
            })
    }

    fun onSpinnerItemSelected(provinces: List<String>, position: Int) {
        if (provinces.isNotEmpty()) {
            itemsSelected.clear()
            itemsSelectedCount = 0
            if (provinces[position] != view.getDefaultValue()) {
                val filterList =
                    cities.data.find { element -> element.province == provinces[position] }
                itemSpinnerSelected = filterList
                view.run {
                    toggleConfirmButtonStatus(true)
                    toggleCitiesData(true)
                    showCitiesData(filterList!!.cities)
                }
            } else {
                itemSpinnerSelected = null
                view.run {
                    toggleCitiesData(false)
                    toggleConfirmButtonStatus(false)
                }
            }
        }
    }

    fun onItemListClicked(position: Int, active: Boolean) {
        val checkActive = if (!active) {
            itemsSelectedCount--
            val city =
                itemsSelected.find { item -> item.city == itemSpinnerSelected!!.cities[position].city }
            itemsSelected.remove(city!!)
            active
        } else if (itemsSelectedCount >= 5) {
            view.showMaxLimitReachedError()
            false
        } else {
            itemsSelectedCount++
            itemsSelected.add(itemSpinnerSelected!!.cities[position])
            active
        }
        itemSpinnerSelected!!.cities[position].selected = checkActive
        view.showCitiesData(itemSpinnerSelected!!.cities)
    }

    fun onButtonConfirmClicked() {
        with(itemSpinnerSelected!!) {
            view.goToCurrentWeatherActivity(
                this@CitySelectionPresenter.cities.country,
                this.province,
                this.capital,
                itemsSelected
            )
        }
    }
}