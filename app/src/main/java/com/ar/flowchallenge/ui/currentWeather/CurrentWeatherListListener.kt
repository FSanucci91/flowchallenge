package com.ar.flowchallenge.ui.currentWeather

interface CurrentWeatherListListener {
    fun onItemClick(position: Int)
}