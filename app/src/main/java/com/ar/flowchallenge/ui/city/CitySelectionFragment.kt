package com.ar.flowchallenge.ui.city

import android.content.Context
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.ar.flowchallenge.R
import com.ar.flowchallenge.core.BaseFragment
import com.ar.flowchallenge.model.city.ModelCity
import com.ar.flowchallenge.model.currentWeather.ModelCurrentWeather
import com.ar.flowchallenge.ui.currentWeather.CurrentWeatherActivity
import kotlinx.android.synthetic.main.fragment_city_selection.*
import kotlinx.android.synthetic.main.layout_error.*
import org.koin.android.scope.currentScope
import org.koin.core.parameter.parametersOf

class CitySelectionFragment : BaseFragment<CitySelectionPresenter>(), CitySelectionView,
    AdapterView.OnItemSelectedListener {

    override val presenter: CitySelectionPresenter by currentScope.inject { parametersOf(this) }

    override val layout: Int = R.layout.fragment_city_selection

    private var provinces = listOf<String>()
    private lateinit var listAdapter: CityListAdapter

    override fun init() {

        listAdapter = CityListAdapter(mutableListOf(), object : CityListListener {
            override fun onItemClick(position: Int, active: Boolean) {
                presenter.onItemListClicked(position, active)
            }
        })

        vRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
        }
    }

    override fun setListeners() {
        vProvinceSpinner.onItemSelectedListener = this

        vConfirmBtn.setOnClickListener {
            presenter.onButtonConfirmClicked()
        }

        vErrorButton.setOnClickListener {
            CitySelectionActivity.start(requireContext())
            requireActivity().finish()
        }
    }

    override fun toggleProgressBar(status: Boolean) {
        vProgressBar.visibility = if (status) View.VISIBLE else View.GONE
        vConfirmBtn.visibility = if (status) View.GONE else View.VISIBLE
        vProvinceSpinner.visibility = if (status) View.GONE else View.VISIBLE
    }

    override fun getCitySelectionContext(): Context = requireContext()

    override fun toggleDataProgressBar(status: Boolean) {
        vDataProgressBar.visibility = if (status) View.VISIBLE else View.GONE
    }

    override fun showSpinnerData(data: List<String>) {
        provinces = data
        val arrayAdapter = ArrayAdapter(requireContext(), R.layout.spinner_item, data).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        vProvinceSpinner.adapter = arrayAdapter
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        presenter.onSpinnerItemSelected(provinces, position)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun showJsonParseError() {
        showError(getString(R.string.city_json_error))
    }

    private fun showError(message: String) {
        vDataProgressBar.visibility = View.GONE
        vTitle.visibility = View.GONE
        vConfirmBtn.visibility = View.GONE
        vProvinceSpinner.visibility = View.GONE
        vRecyclerView.visibility = View.GONE
        vErrorContainer.visibility = View.VISIBLE
        vSubtitleError.text = message
        vErrorButton.text = getString(R.string.city_error_button)
    }

    override fun toggleConfirmButtonStatus(active: Boolean) {
        vConfirmBtn.isEnabled = active
    }

    override fun toggleCitiesData(active: Boolean) {
        vRecyclerView.visibility = if (active) View.VISIBLE else View.GONE
    }

    override fun showCitiesData(cities: List<ModelCity>) {
        listAdapter.updateCardIssuers(cities)
    }

    override fun showMaxLimitReachedError() {
        toastFactory.show(R.string.city_max_limit_reached_error)
    }

    override fun getDefaultValue(): String = getString(R.string.city_default)

    override fun goToCurrentWeatherActivity(
        country: String,
        province: String,
        capital: String,
        itemsSelected: List<ModelCity>
    ) {
        val argumentData =
            ModelCurrentWeather(
                country,
                province,
                capital,
                itemsSelected
            )
        CurrentWeatherActivity.start(requireContext(), argumentData)
    }
}