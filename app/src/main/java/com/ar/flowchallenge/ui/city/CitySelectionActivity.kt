package com.ar.flowchallenge.ui.city

import android.content.Context
import android.content.Intent
import com.ar.flowchallenge.R
import com.ar.flowchallenge.core.BaseActivity
import org.koin.android.ext.android.inject

class CitySelectionActivity : BaseActivity() {
    private val fragment: CitySelectionFragment by inject()

    override fun layout(): Int = R.layout.activity_base

    override fun init() = replaceFragment(R.id.vActivityBaseContent, fragment)

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, CitySelectionFragment::class.java)
            context.startActivity(starter)
        }
    }
}