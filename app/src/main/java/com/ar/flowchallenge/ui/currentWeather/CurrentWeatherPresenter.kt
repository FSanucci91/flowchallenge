package com.ar.flowchallenge.ui.currentWeather

import com.ar.flowchallenge.core.BasePresenter
import com.ar.flowchallenge.model.currentWeather.ModelCurrentWeather
import com.ar.flowchallenge.model.currentWeather.ModelCurrentWeatherResponse
import com.ar.flowchallenge.network.currentWeather.CurrentWeatherAdapter
import com.ar.flowchallenge.network.currentWeather.CurrentWeatherListener

class CurrentWeatherPresenter(
    override val view: CurrentWeatherView
) : BasePresenter<CurrentWeatherView> {

    private lateinit var currentWeatherData: ModelCurrentWeather
    var adapter = CurrentWeatherAdapter()
    private val weatherList = mutableListOf<ModelCurrentWeatherResponse>()
    private var serviceError = false

    override fun onAttachView() {
        view.toggleProgressBar(true)
        currentWeatherData = view.getArgumentData()
        val cityData = mutableListOf<String>()
        if (currentWeatherData.capital.isNotEmpty()) {
            cityData.add(currentWeatherData.capital)
        }
        currentWeatherData.cities.forEach { item ->
            cityData.add(item.city)
        }
        getDataFromService(cityData)
    }

    private fun getDataFromService(cityList: MutableList<String>) {
        if (cityList.isNotEmpty()) {
            val city = cityList.removeAt(0)
            adapter.currentWeatherService(currentWeatherData.country, city,
                object : CurrentWeatherListener {
                    override fun onSuccessResponse(data: ModelCurrentWeatherResponse) {
                        weatherList.add(data)
                        getDataFromService(cityList)
                    }

                    override fun onEmptyResponse() {
                        serviceError = true
                    }

                    override fun onErrorResponse() {
                        serviceError = true
                    }
                })
        } else {
            with(view) {
                if (weatherList.isEmpty()) {
                    showServiceError()
                } else {
                    if (serviceError) showServicePartialError()
                    showWeatherData(weatherList)
                }
                toggleProgressBar(false)
            }
        }
    }

    fun onCardItemButtonClicked(position: Int) {
        view.goToWeatherDetail(weatherList[position].id)
    }
}