package com.ar.flowchallenge.ui.currentWeather

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.ar.flowchallenge.R
import com.ar.flowchallenge.core.BaseActivity
import com.ar.flowchallenge.model.city.ModelCity
import com.ar.flowchallenge.model.currentWeather.ModelCurrentWeather

class CurrentWeatherActivity : BaseActivity() {

    override fun layout(): Int = R.layout.activity_base

    override fun init() {
        val arguments = Bundle().apply {
            with(intent) {
                putString(COUNTRY, getStringExtra(COUNTRY))
                putString(PROVINCE, getStringExtra(PROVINCE))
                putString(CAPITAL, getStringExtra(CAPITAL))
                putSerializable(CITIES, getStringArrayListExtra(CITIES))
            }
        }
        val fragment: CurrentWeatherFragment =
            CurrentWeatherFragment.newInstance(baseContext, arguments)
        replaceFragment(R.id.vActivityBaseContent, fragment)
    }

    override fun onBackPressed() {
        finish()
    }

    companion object {
        const val COUNTRY = "country"
        const val PROVINCE = "province"
        const val CAPITAL = "capital"
        const val CITIES = "cities"

        fun start(context: Context, arguments: ModelCurrentWeather) {
            val starter = Intent(context, CurrentWeatherActivity::class.java).apply {
                putExtra(COUNTRY, arguments.country)
                putExtra(PROVINCE, arguments.province)
                putExtra(CAPITAL, arguments.capital)
                if (arguments.cities.isNotEmpty()) {
                    val arrayList = arrayListOf<ModelCity>().apply {
                        addAll(arguments.cities)
                    }
                    putExtra(CITIES, arrayList)
                }
            }
            context.startActivity(starter)
        }
    }
}