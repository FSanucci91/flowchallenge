package com.ar.flowchallenge.ui.weatherDetail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.ar.flowchallenge.R
import com.ar.flowchallenge.core.BaseActivity

class WeatherDetailActivity : BaseActivity() {

    override fun layout(): Int = R.layout.activity_base

    override fun init() {
        val arguments = Bundle().apply {
            putInt(CITY_ID, intent.getIntExtra(CITY_ID, 0))
        }
        val fragment: WeatherDetailFragment =
            WeatherDetailFragment.newInstance(baseContext, arguments)
        replaceFragment(R.id.vActivityBaseContent, fragment)
    }

    override fun onBackPressed() {
        finish()
    }

    companion object {
        const val CITY_ID = "city_id"

        fun start(context: Context, cityId: Int) {
            val starter = Intent(context, WeatherDetailActivity::class.java).apply {
                putExtra(CITY_ID, cityId)
            }
            context.startActivity(starter)
        }
    }
}