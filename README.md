# FlowChallenge

## <a name="title"></a> Developer Applicant Interview Test

## <a name="summary"></a>Summary

La siguiente prueba plantea el desarrollo de una aplicación de consulta de clima que pueda visualizar el pronóstico actual, próximos 5 días para la ciudad actual y de otras 5 ciudades. 

Además del desarrollo específico de las funcionalidades, se requiere identificar y generar los casos de test que se consideren necesarios.

La entrega del código se espera que se entregue en algún repositorio público (Por ejemplo Github).

Se recomienda utilizar servicio API de clima Open Weather Map, pero se puede usar utilizar cualquier otro de preferencia.

## <a name="recommendations"></a> Sugerencia de servicios/librerías:
*  Retrofit 2 (recomendado)
*  Support API
*  Glide/Picasso
*  IP-API

Se pueden usar cualquier otras librerías que consideren de utilidad.

## <a name="test"></a> Frontend Test

Preferentemente desarrollar en Kotlin, alternativamente desarrollarlo bajo Java 8.

Desarrollar una App Android que permita visualizar el pronóstico climático actual y de los próximos 5 días en la ubicación actual y permita visualizar el pronóstico de otras 5 ciudades seleccionables.

1. Se debe idear y diseñar la UI que se considere más acorde (si bien aporta al ejercicio, no se valoran habilidades de diseño, sino de uso de componentes).
2. Los datos deben ser consumidos de la API seleccionada.
3. Se evaluará la optimización y la utilización de recursos de la UI
4. Se evaluará el manejo de Threads/networking

Realizar el test con total libertad, serán apreciados conocimientos adicionales a los nombrados anteriormente, aunque éstos no son requeridos.

Éxitos!

## <a name="notes"></a> Notas:
El desarrollo incluye lo siguiente (acorde a mi interpretación personal)
1. Para obtener los datos de las provincias se utilizó un json obtenido de internet para agilizar el consumo de datos (si el objetivo es evaluar el consumo de servicios se puede observar tranquilamente en las demás pantallas y de éste modo se agrega una nueva fuente de datos con su respectivo manejo).
2. La selección de ciudades se realizará por provincia. De la provincia seleccionada se visualizará de forma obligatoria la capital de dicha provincia y aparte se dará la opción de seleccionar otras 5 diferentes (no mas).
3. De la lista de ciudades seleccionadas, se mostrará el clima actual de cada una de ellas (de las que existen y se obtienen de forma satisfactoria los datos) y se dará la posibilidad de visualizar el pronóstico extendido por los próximos 5 días.
4. Se optó por elegir la modalidad 5días-cada-3horas, en caso de querer visualizarse de una manera distinta es tan simple como cambiar el endpoint del servicio (la estructura de datos seguirá igual).


